/*
-1 creer une fonction fetchUser
-1-1 comment passer une fonction dans une variable ?
-2 creer une fonction d'affichage userDispaly
 ATTENTION: quand on fait des fetch, on a souvent besoin d' async
-3 traiter les dates

*/

let userData = [];
const fetchUser = async function () {
   await fetch('https://randomuser.me/api/?results=2').then(
    (response)=>response.json()
    ).then(
    (infoUser)=> userData = (infoUser.results)
    );

    //console.log(userData);
    //methode2
        // .then(
        //   ()=>(console.log(userData))
        // )
    //methode1
        // setTimeout(function () { 
        //    console.log(userData); 
        // },1000 );

}
//fetchUser();


const dateParser =  (date) => {
    let newDate = new Date(date).toLocaleDateString("fr-FR",{
        year: "numeric",
        month: "long",
        day: "numeric"
    });
    return newDate;
}

const dayCalc =  (date) => {
    let today = new Date();
    let todayTimestamp = Date.parse(today);
    let timestamp = Date.parse(date);


    return Math.ceil((todayTimestamp-timestamp)/8.64e7) ;
}


const userDispaly = async function () {
    await fetchUser();

    document.body.innerHTML = userData.map(
        (user) =>
        `
        <img src=${user.picture.large}>
        <h3>${user.name.first}</h3>
        <p>${user.location.country}, ${dateParser(user.dob.date)}</p>
        <p>Membre depuis: ${dayCalc(user.registered.date)} jours</p>
        `
    ).join("")





}

userDispaly()
